<?php
function commerce_propay_country_iso_code_3($code) {
	if (strlen($code) == 2) {
		$country_iso_code_3 = array(
			'US' => 'USA',
			'IN' => 'IND',
			'GB' => 'GBR',
			'UM' => 'UMI'
		);
		$code = $country_iso_code_3[$code];
	}
	return $code;
}
